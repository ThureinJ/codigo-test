package tin.thurein.data.local.daos

import android.util.Log
import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single
import tin.thurein.data.common.FileUtils
import tin.thurein.data.local.DatabaseConstants
import tin.thurein.data.local.entities.WonderEntity
import java.io.File

@Dao
abstract class WonderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(wonderEntities: List<WonderEntity>): List<Long>

    @Update
    abstract fun update(wonderEntity: WonderEntity): Single<Int>

    @Query("SELECT * FROM ${DatabaseConstants.WONDER_TABLE_NAME};")
    abstract fun getWonders(): Flowable<List<WonderEntity>>

    @Query("SELECT * FROM ${DatabaseConstants.WONDER_TABLE_NAME};")
    abstract fun getWondersToDelete(): List<WonderEntity>

    @Query("SELECT * FROM ${DatabaseConstants.WONDER_TABLE_NAME} WHERE ${DatabaseConstants.ID} = :id;")
    abstract fun getWonderById(id: Long): Single<WonderEntity>

    @Query("DELETE FROM ${DatabaseConstants.WONDER_TABLE_NAME};")
    abstract fun deleteAll(): Int

    @Transaction
    open fun saveWonders(wonderEntities: List<WonderEntity>): List<Long> {
        val existingWonders = getWondersToDelete()
        existingWonders.forEach { wonderEntity ->
            val file = File(FileUtils.getGlobalFilePath() + wonderEntity.imageName)
            if (file.exists() && file.delete()) {
                Log.e("File Deleted", file.absolutePath)
            }
        }
        deleteAll()
        return insert(wonderEntities)
    }
}