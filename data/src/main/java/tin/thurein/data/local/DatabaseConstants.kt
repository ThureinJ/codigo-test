package tin.thurein.data.local

object DatabaseConstants {
    const val WONDER_TABLE_NAME = "wonder"

    const val ID = "id"

    const val LOCATION = "location"

    const val DESCRIPTION = "description"

    const val IMAGE_URL = "image_url"

    const val IMAGE_NAME = "image_name"

    const val LAT = "latitude"

    const val LONG = "longitude"
}