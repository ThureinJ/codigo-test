package tin.thurein.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import tin.thurein.data.local.daos.WonderDao
import tin.thurein.data.local.entities.WonderEntity

@Database(entities = [WonderEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun wonderDao(): WonderDao
}