package tin.thurein.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import tin.thurein.data.local.DatabaseConstants
import tin.thurein.domain.models.Wonder

@Entity(tableName = DatabaseConstants.WONDER_TABLE_NAME)
class WonderEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DatabaseConstants.ID)
    var id: Long? = null

    @ColumnInfo(name = DatabaseConstants.LOCATION)
    var location: String? = null

    @ColumnInfo(name = DatabaseConstants.DESCRIPTION)
    var description: String? = null

    @ColumnInfo(name = DatabaseConstants.IMAGE_URL)
    var imageUrl: String? = null

    @ColumnInfo(name = DatabaseConstants.IMAGE_NAME)
    var imageName: String? = null

    @ColumnInfo(name = DatabaseConstants.LAT)
    var lat: Double? = null

    @ColumnInfo(name = DatabaseConstants.LONG)
    var longitude: Double? = null

    constructor() {}

    constructor(id: Long?, location: String?, description: String?, imageUrl: String?, imageName: String?, lat: Double?, long: Double?){
        this.id = id
        this.location = location
        this.description = description
        this.imageUrl = imageUrl
        this.imageName = imageName
        this.lat = lat
        this.longitude = long
    }

    fun toWonder() = Wonder(id, location, description, imageUrl, imageName, lat, longitude)

    companion object {
        fun fromWonder(wonder: Wonder): WonderEntity {
            return WonderEntity(wonder.id, wonder.location, wonder.description, wonder.imageUrl, wonder.imageName, wonder.lat, wonder.long)
        }
    }
}
