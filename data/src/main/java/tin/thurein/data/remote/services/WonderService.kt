package tin.thurein.data.remote.services

import io.reactivex.Observable
import retrofit2.http.GET
import tin.thurein.data.remote.models.WondersData

interface WonderService {
    @GET("13g69v")
    fun getRemoteWonders(): Observable<WondersData>
}