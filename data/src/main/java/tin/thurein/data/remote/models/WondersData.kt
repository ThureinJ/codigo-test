package tin.thurein.data.remote.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WondersData(
    @Expose
    @SerializedName("wonders")
    var wonders: List<WonderData>
)