package tin.thurein.data.remote.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import tin.thurein.domain.models.Wonder

data class WonderData(
    @Expose
    @SerializedName("location")
    var location: String?,
    @Expose
    @SerializedName("description")
    var description: String?,
    @Expose
    @SerializedName("image")
    var imageURL: String?,
    @Expose
    @SerializedName("lat")
    var lat: Double?,
    @Expose
    @SerializedName("long")
    var long: Double?
) {
    fun toWonder(): Wonder = Wonder(null, location, description, imageURL, null, lat, long)
}