package tin.thurein.data.usecases

import io.reactivex.Single
import tin.thurein.domain.models.Wonder
import tin.thurein.domain.repositories.WonderRepository
import tin.thurein.domain.usecases.GetWonderDetailUseCase
import javax.inject.Inject

class GetWonderDetailUseCaseImpl: GetWonderDetailUseCase {
    private val wonderRepository: WonderRepository

    @Inject
    constructor(wonderRepository: WonderRepository) {
        this.wonderRepository = wonderRepository
    }
    override fun getWonderById(id: Long): Single<Wonder> {
        return wonderRepository.getLocalWonderById(id)
    }
}