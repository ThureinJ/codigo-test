package tin.thurein.data.usecases

import android.util.Log
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import tin.thurein.data.common.FileUtils
import tin.thurein.domain.models.Wonder
import tin.thurein.domain.repositories.WonderRepository
import tin.thurein.domain.usecases.GetWondersUseCase
import java.util.*
import javax.inject.Inject

class GetWondersUseCaseImpl: GetWondersUseCase {
    private val wonderRepository: WonderRepository

    @Inject
    constructor(wonderRepository: WonderRepository) {
        this.wonderRepository = wonderRepository
    }

    override fun getWonders(): Flowable<List<Wonder>> {
        getRemoteWonders()
        return wonderRepository.getLocalWonders()
    }

    private fun getRemoteWonders() {
        wonderRepository.getRemoteWonders()
            .observeOn(Schedulers.computation())
            .subscribeOn(Schedulers.io())
            .subscribe(object: Observer<List<Wonder>>{
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(wonders: List<Wonder>) {
                    saveWonders(wonders)
                }

                override fun onError(e: Throwable) {
                    Log.e("Error", "OK ${e.message}")
                }
            })
    }

    private fun saveWonders(wonders: List<Wonder>) {
        saveAndDownloadFile(wonders)
            .observeOn(Schedulers.computation())
            .subscribeOn(Schedulers.io())
            .subscribe(object : Observer<List<Wonder>>{
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: List<Wonder>) {
                    Log.e("SaveSuccess", "OK")
                    wonderRepository.saveLocalWonders(t)
                }

                override fun onError(e: Throwable) {
                    Log.e("OnError", "${e.message}")
                }
            })
    }

    private fun saveAndDownloadFile(wonders: List<Wonder>): Observable<List<Wonder>> {
        return Observable.create { emitter: ObservableEmitter<List<Wonder>> ->
            try {
                wonders.forEach { wonder ->
                    wonder.imageUrl?.let {
                        val imageName = Date().time.toString()
                        wonder.imageName = FileUtils.downloadAndSaveImage(it, imageName)
                    }
                }
                emitter.onNext(wonders)
            } catch (e: Exception) {
                emitter.onError(e)
            } finally {
                emitter.onComplete()
            }
        }
    }
}