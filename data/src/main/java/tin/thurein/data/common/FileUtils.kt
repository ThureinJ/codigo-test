package tin.thurein.data.common

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.Log
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


object FileUtils {
    private fun saveImage(image: Bitmap, imageName: String): String? {
        var savedImagePath: String? = null
        val imageFileName = "JPEG_$imageName.jpg"
        val storageDir = File(getGlobalFilePath())
        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            val imageFile = File(storageDir, imageFileName)

            if(imageFile.exists()) {
                imageFile.delete()
            }

            savedImagePath = imageFile.absolutePath
            try {
                val fOut: OutputStream = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
                return imageFileName
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Save", "e : ${e.message}")
            }
            // Add the image to the system gallery
//            galleryAddPic(savedImagePath, mContext)
        }
        return savedImagePath
    }

    private fun galleryAddPic(imagePath: String?, context: Context) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(imagePath)
        val contentUri: Uri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        context.sendBroadcast(mediaScanIntent)
    }

    private fun getBitmapFromUrl(urlString: String): Bitmap? {
        val url: URL = URL(urlString)
        var connection: HttpURLConnection? = null
        try { // Initialize a new http url connection
            connection = url.openConnection() as HttpURLConnection
            // Connect the http url connection
            connection.connect()
            // Get the input stream from http url connection
            val inputStream: InputStream = connection.inputStream
            /*
                    BufferedInputStream
                        A BufferedInputStream adds functionality to another input stream-namely,
                        the ability to buffer the input and to support the mark and reset methods.
                */
            /*
                    BufferedInputStream(InputStream in)
                        Creates a BufferedInputStream and saves its argument,
                        the input stream in, for later use.
                */
            // Initialize a new BufferedInputStream from InputStream
            val bufferedInputStream =
                BufferedInputStream(inputStream)
            /*
                    decodeStream
                        Bitmap decodeStream (InputStream is)
                            Decode an input stream into a bitmap. If the input stream is null, or
                            cannot be used to decode a bitmap, the function returns null. The stream's
                            position will be where ever it was after the encoded data was read.

                        Parameters
                            is InputStream : The input stream that holds the raw data
                                              to be decoded into a bitmap.
                        Returns
                            Bitmap : The decoded bitmap, or null if the image data could not be decoded.
                */
        // Convert BufferedInputStream to Bitmap object
            // Return the downloaded bitmap
            return BitmapFactory.decodeStream(bufferedInputStream)
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e("getBitmap", "Ex : ${e.message}")
        } finally { // Disconnect the http url connection
            connection?.disconnect()
        }
        return null
    }

    fun downloadAndSaveImage(urlString: String, imageName: String): String? {
        val bitmap = getBitmapFromUrl(urlString)
        bitmap?.let { return saveImage(it, imageName) }
        return null
    }

    fun getGlobalFilePath(): String = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        .toString() + Constant.FILE_PATH
}