package tin.thurein.data.repositories

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import tin.thurein.data.local.AppDatabase
import tin.thurein.data.local.daos.WonderDao
import tin.thurein.data.local.entities.WonderEntity
import tin.thurein.data.remote.ApiBuilder
import tin.thurein.data.remote.models.WonderData
import tin.thurein.data.remote.models.WondersData
import tin.thurein.data.remote.services.WonderService
import tin.thurein.domain.models.Wonder
import tin.thurein.domain.repositories.WonderRepository
import javax.inject.Inject

class WonderRepositoryImpl: WonderRepository {
    private val appDatabase: AppDatabase
    private val apiBuilder: ApiBuilder
    private val wonderDao: WonderDao

    @Inject
    constructor(appDatabase: AppDatabase, apiBuilder: ApiBuilder) {
        this.appDatabase = appDatabase
        this.apiBuilder = apiBuilder
        wonderDao = appDatabase.wonderDao()
    }

    override fun getRemoteWonders(): Observable<List<Wonder>> {
        return apiBuilder.getService(WonderService::class.java).getRemoteWonders()
            .map { wondersData: WondersData -> wondersData.wonders.map { wonderData: WonderData -> wonderData.toWonder() }}
    }

    override fun getLocalWonders(): Flowable<List<Wonder>> {
        return wonderDao.getWonders().map { wonderEntities -> wonderEntities.map { wonderEntity -> wonderEntity.toWonder() } }
    }

    override fun getLocalWonderById(id: Long): Single<Wonder> {
        return wonderDao.getWonderById(id).map { it.toWonder() }
    }

    override fun saveLocalWonders(wonders: List<Wonder>): List<Long> {
        return wonderDao.saveWonders(wonders.map { wonder ->
            WonderEntity.fromWonder(wonder) })
    }
}