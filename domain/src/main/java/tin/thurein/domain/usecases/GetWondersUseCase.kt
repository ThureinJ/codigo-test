package tin.thurein.domain.usecases

import io.reactivex.Flowable
import tin.thurein.domain.models.Wonder

interface GetWondersUseCase {
    fun getWonders(): Flowable<List<Wonder>>
}