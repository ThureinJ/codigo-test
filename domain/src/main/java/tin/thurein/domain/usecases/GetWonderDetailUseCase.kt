package tin.thurein.domain.usecases

import io.reactivex.Single
import tin.thurein.domain.models.Wonder

interface GetWonderDetailUseCase {
    fun getWonderById(id: Long): Single<Wonder>
}