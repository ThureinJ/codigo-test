package tin.thurein.domain.repositories

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import tin.thurein.domain.models.Wonder

interface WonderRepository {
    fun getRemoteWonders(): Observable<List<Wonder>>

    fun getLocalWonders(): Flowable<List<Wonder>>

    fun getLocalWonderById(id: Long): Single<Wonder>

    fun saveLocalWonders(wonders: List<Wonder>): List<Long>
}