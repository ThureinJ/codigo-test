package tin.thurein.domain.models

data class Wonder(
    var id: Long?,
    var location: String?,
    var description: String?,
    var imageUrl: String?,
    var imageName: String?,
    var lat: Double?,
    var long: Double?) {
    fun getLongitude(): String = "latitude : $long"

    fun getLatitude(): String = "longitude : $lat"
}