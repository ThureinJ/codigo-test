package tin.thurein.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    /**
     * format date to string
     *
     * @param format
     * @param date
     * @return date string in format
     */
    fun formatDate(format: String?, date: Date?): String? {
        return try {
            val sdf = SimpleDateFormat(format)
            sdf.format(date)
        } catch (e: Exception) {
            System.err.println("While formatting date : " + e.message)
            null
        }
    }

    /**
     * parse string to date
     *
     * @param format
     * @param dateStr
     * @return date from string in format
     */
    fun parseToDate(format: String?, dateStr: String?): Date? {
        return try {
            val sdf = SimpleDateFormat(format)
            sdf.parse(dateStr)
        } catch (ex: Exception) {
            System.err.println("While parsing to date : " + ex.message)
            null
        }
    }

    /**
     * change date in from format to to format
     *
     * @param fromFormat
     * @param toFormat
     * @param dateStr
     * @return
     */
    fun changeDateFormat(
        fromFormat: String?,
        toFormat: String?,
        dateStr: String?
    ): String? {
        return try {
            val from = SimpleDateFormat(fromFormat)
            val date = from.parse(dateStr)
            val to = SimpleDateFormat(toFormat)
            to.format(date)
        } catch (e: Exception) {
            System.err.println("While changing date format : " + e.message)
            null
        }
    }

    /**
     * format date to string including locale
     *
     * @param format
     * @param date
     * @param locale
     * @return
     */
    fun formatDate(
        format: String?,
        date: Date?,
        locale: Locale?
    ): String? {
        return try {
            val sdf = SimpleDateFormat(format, locale)
            sdf.format(date)
        } catch (e: Exception) {
            System.err.println("While formatting date : " + e.message)
            null
        }
    }

    /**
     * parse string in format to date including locale
     *
     * @param format
     * @param dateStr
     * @param locale
     * @return
     */
    fun parseToDate(
        format: String?,
        dateStr: String?,
        locale: Locale?
    ): Date? {
        return try {
            val sdf = SimpleDateFormat(format, locale)
            sdf.parse(dateStr)
        } catch (ex: Exception) {
            System.err.println("While parsing to date : " + ex.message)
            null
        }
    }

    /**
     * change date string from from-format to to-format including locale
     *
     * @param fromFormat
     * @param toFormat
     * @param dateStr
     * @param locale
     * @return
     */
    fun changeDateFormat(
        fromFormat: String?,
        toFormat: String?,
        dateStr: String?,
        locale: Locale?
    ): String? {
        return try {
            val from = SimpleDateFormat(fromFormat, locale)
            val date = from.parse(dateStr)
            val to = SimpleDateFormat(toFormat, locale)
            to.format(date)
        } catch (e: Exception) {
            System.err.println("While changing date format : " + e.message)
            null
        }
    }
}