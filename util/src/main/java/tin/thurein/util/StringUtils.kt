package tin.thurein.util

object StringUtils {
    /**
     * check string is empty if null it returns true
     *
     * @param str
     * @return
     */
    fun isEmpty(str: String?): Boolean {
        return str == null || str.isEmpty()
    }
}