package tin.thurein.util

object DateFormat {
    const val CALENDAR_DATETIME_DEFAULT_FORMAT = "yyyyMMdd HHmmss"
    const val CALENDAR_DATETIME_DEFAULT_FORMAT_2 = "yyyy-MM-dd HH:mm:ss.S"
    const val CALENDAR_DATETIME_DEFAULT_FORMAT_3 = "dd/MM/yyyy hh:mm a"
    const val CALENDAR_DATETIME_DEFAULT_FORMAT_4 = "yyyy-MM-dd HH:mm:ss"
    const val CALENDAR_DATE_DEFAULT_FORMAT = "yyyyMMdd"
    const val CALENDAR_DATE_DEFAULT_FORMAT2 = "dd/MM/yyyy"
    const val CALENDAR_DATE_DEFAULT_FORMAT3 = "dd-MM-yyyy"
    const val CALENDAR_DATE_DEFAULT_FORMAT4 = "yyyy-MM-dd"
    const val CALENDAR_TIME_DEFAULT_FORMAT = "HHmmss"
    const val CALENDAR_TIME_DEFAULT_FORMAT2 = "HHmm"
    const val CALENDAR_TIME_DEFAULT_FORMAT3 = "HH:mm:ss"
    const val FULL_MONTH_YEAR_FORMAT = "MMMM yyyy"
}