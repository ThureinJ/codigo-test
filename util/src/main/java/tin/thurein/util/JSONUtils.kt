package tin.thurein.util

import com.google.gson.GsonBuilder
import org.apache.commons.lang3.reflect.TypeUtils
import org.json.JSONArray
import org.json.JSONException
import java.lang.reflect.Type
import java.util.*

object JSONUtils {
    private var gson = GsonBuilder()
        .setPrettyPrinting()
        .serializeNulls()
        .create()
    /**
     * Convert Json Array to List
     *
     * @param aClass
     * @param jsonArray
     * @return
     */
    fun <T> convertJsonArrayToList(
        aClass: Class<T>?,
        jsonArray: String?
    ): List<T> {
        val type: Type =
            TypeUtils.parameterize(ArrayList::class.java, aClass)
        return gson!!.fromJson(jsonArray, type)
    }

    /**
     * Convert List to json array
     *
     * @param tempList
     * @return
     */
    fun <T> convertListToJsonArray(tempList: List<T>?): JSONArray? {
        return try {
            JSONArray(gson!!.toJson(tempList))
        } catch (e: JSONException) {
            System.err.println("JSONException : " + e.message)
            null
        }
    }

    /**
     * convert json string to object
     *
     * @param aClass
     * @param jsonString
     * @param <T>
     * @return
    </T> */
    fun <T> convertJsonToObject(aClass: Class<T>?, jsonString: String?): T {
        return gson!!.fromJson(jsonString, aClass)
    }

    /**
     * convert object to json string
     *
     * @param object
     * @param <T>
     * @return
    </T> */
    fun <T> convertObjectToJsonString(`object`: T): String {
        return gson!!.toJson(`object`)
    }
}