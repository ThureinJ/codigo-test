package tin.thurein.codigo_test.models

enum class NetworkState {
    CONNECTED,
    NOT_CONNECTED;
}