package tin.thurein.codigo_test.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import tin.thurein.codigo_test.activities.MainActivity
import tin.thurein.codigo_test.activities.WonderDetailActivity

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindWonderDetailActivity(): WonderDetailActivity
}