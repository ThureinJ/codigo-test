package tin.thurein.codigo_test.injection

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import tin.thurein.codigo_test.viewModels.MainViewModel
import tin.thurein.codigo_test.viewModels.WonderDetailViewModel
import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModle(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WonderDetailViewModel::class)
    abstract fun bindWonderDetailViewModel(wonderDetailViewModel: WonderDetailViewModel): ViewModel

    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)
}