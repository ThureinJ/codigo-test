package tin.thurein.codigo_test.injection

import dagger.Binds
import dagger.Module
import tin.thurein.data.repositories.WonderRepositoryImpl
import tin.thurein.domain.repositories.WonderRepository

@Module
interface RepositoryModule {
    @Binds
    fun bindWonderRepository(wonderRepositoryImpl: WonderRepositoryImpl): WonderRepository
}