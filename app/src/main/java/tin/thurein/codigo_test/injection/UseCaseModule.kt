package tin.thurein.codigo_test.injection

import dagger.Binds
import dagger.Module
import tin.thurein.data.usecases.*
import tin.thurein.domain.usecases.*

@Module
interface UseCaseModule {
    @Binds
    fun bindGetWondersUseCase(getWonderUseCaseImpl: GetWondersUseCaseImpl): GetWondersUseCase

    @Binds
    fun bindGetWonderDetailUseCase(getWonderDetailUseCaseImpl: GetWonderDetailUseCaseImpl): GetWonderDetailUseCase
}