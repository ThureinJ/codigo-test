package tin.thurein.codigo_test.injection

import android.app.Application
import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import tin.thurein.data.local.AppDatabase
import tin.thurein.data.remote.ApiBuilder
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "app").build()
    }

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideApiBuilder(): ApiBuilder {
        return ApiBuilder
    }
}