package tin.thurein.codigo_test.activities

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import tin.thurein.codigo_test.R
import tin.thurein.codigo_test.databinding.ActivityWonderDetailBinding
import tin.thurein.codigo_test.injection.ViewModelFactory
import tin.thurein.codigo_test.viewModels.WonderDetailViewModel
import tin.thurein.domain.models.ModelWrapper
import tin.thurein.domain.models.Progress
import tin.thurein.domain.models.Wonder
import tin.thurein.util.JSONUtils
import javax.inject.Inject

class WonderDetailActivity : BasedActivity() {

    @JvmField
    @Inject
    var viewModelFactory: ViewModelFactory? = null

    private lateinit var vm: WonderDetailViewModel

    private lateinit var binding: ActivityWonderDetailBinding

    private var wonder: Wonder? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wonder_detail)
        AndroidInjection.inject(this)

        val savedData = savedInstanceState?.getString("data")
        savedData?.let{
            wonder = JSONUtils.convertJsonToObject(Wonder::class.java, it)
        }

        initData()
    }

    private fun initData() {
        vm = ViewModelProvider(this, viewModelFactory as ViewModelProvider.Factory).get(WonderDetailViewModel::class.java)
        vm.setTitle("Wonder Detail")
        vm.setBackVisibility(View.VISIBLE)
        binding.vm = vm

        if (wonder == null) {
            vm.getWonder(intent.getLongExtra("data", 0))
        }
        vm.mutableLiveData.observe(this, Observer { handleWonder(it) })
        vm.backPressedMutableLiveData.observe(this, Observer { handleBackPress() })
    }

    private fun handleWonder(modelWrapper: ModelWrapper<Wonder>) {
        when(modelWrapper.progress) {
            Progress.SUCCESS -> {
                wonder = modelWrapper.model
            }

            Progress.ERROR -> {
                toast(modelWrapper.message)
            }
        }
    }

    private fun handleBackPress() {
        onBackPressed()
    }

    override fun networkAvailable() {
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("data", JSONUtils.convertObjectToJsonString(wonder))
    }
}
