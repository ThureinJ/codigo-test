package tin.thurein.codigo_test.activities

import android.content.Intent
import android.content.IntentFilter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import tin.thurein.codigo_test.delegates.NetworkStateDelegate
import tin.thurein.codigo_test.models.NetworkState
import tin.thurein.codigo_test.receivers.NetworkStateReceiver

abstract class BasedActivity: AppCompatActivity(), NetworkStateDelegate {

    private val receiver = NetworkStateReceiver()
    override fun onResume() {
        super.onResume()
        receiver.networkStateDelegate = this
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(receiver, intentFilter)
    }
    override fun onNetworkChanged(networkState: NetworkState) {
        when(networkState) {
            NetworkState.CONNECTED -> networkAvailable()

            NetworkState.NOT_CONNECTED -> toast("No internet connection.")
        }
    }

    abstract fun networkAvailable()

    protected fun toast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }
}