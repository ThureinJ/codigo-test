package tin.thurein.codigo_test.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import tin.thurein.codigo_test.R
import tin.thurein.codigo_test.adapters.WonderAdapter
import tin.thurein.codigo_test.common.NetworkUtils
import tin.thurein.codigo_test.common.PermissionUtils
import tin.thurein.codigo_test.databinding.ActivityMainBinding
import tin.thurein.codigo_test.delegates.WonderAdapterDelegate
import tin.thurein.codigo_test.injection.ViewModelFactory
import tin.thurein.codigo_test.viewModels.MainViewModel
import tin.thurein.domain.models.ModelWrapper
import tin.thurein.domain.models.Progress
import tin.thurein.domain.models.Wonder
import tin.thurein.util.JSONUtils
import javax.inject.Inject

class MainActivity : BasedActivity(), WonderAdapterDelegate {

    @JvmField
    @Inject
    var viewModelFactory: ViewModelFactory? = null

    private var mainViewModel: MainViewModel? = null

    private lateinit var adapter: WonderAdapter

    private lateinit var binding: ActivityMainBinding

    private var wonders: List<Wonder> = ArrayList()

    private var isGranted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        AndroidInjection.inject(this)

        val savedData = savedInstanceState?.getString("data")
        savedData?.let { wonders = JSONUtils.convertJsonArrayToList(Wonder::class.java, it) }
        initUI()
        initData()
        if (PermissionUtils.checkStoragePermissions(this)) {
            isGranted = true
        }
    }

    private fun initData() {
        mainViewModel = ViewModelProvider(this, viewModelFactory as ViewModelProvider.Factory).get(MainViewModel::class.java)
        mainViewModel?.setTitle("Wonders")
        mainViewModel?.setBackVisibility(View.GONE)
        binding.vm = mainViewModel

        mainViewModel?.mutableLiveData?.observe(this, Observer { handleWonders(it) })
    }

    private fun initUI() {
        adapter = WonderAdapter(this)
        binding.rvWonders.adapter = adapter

        binding.srlRefresh.setOnRefreshListener { getWonders() }
    }

    private fun handleWonders(modelWrapper: ModelWrapper<List<Wonder>>) {
        when(modelWrapper.progress){
            Progress.LOADING -> {
                binding.srlRefresh.isRefreshing = true
                toast(modelWrapper.message)
            }
            Progress.SUCCESS -> {
                wonders = modelWrapper.model!!
                adapter.setWonders(wonders)
                binding.srlRefresh.isRefreshing = false
            }
            Progress.ERROR -> {
                binding.srlRefresh.isRefreshing = false
                toast(modelWrapper.message)
            }
        }
    }

    override fun networkAvailable() {
        if(isGranted && wonders.isEmpty()) {
            mainViewModel?.getWonders()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            PermissionUtils.MY_PERMISSIONS_REQUEST_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isGranted = true
                    getWonders()
                }
            }
        }
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, WonderDetailActivity::class.java)
        intent.putExtra("data", wonders[position].id)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("data", JSONUtils.convertListToJsonArray(wonders).toString())
    }

    private fun getWonders() {
        if (isGranted && NetworkUtils.isNetworkAvailable(this) && wonders.isEmpty()) {
            mainViewModel?.getWonders()
        } else {
            binding.srlRefresh.isRefreshing = false
        }
    }
}
