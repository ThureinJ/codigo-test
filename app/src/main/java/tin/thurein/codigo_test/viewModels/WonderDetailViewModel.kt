package tin.thurein.codigo_test.viewModels

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import tin.thurein.data.common.FileUtils
import tin.thurein.domain.models.ModelWrapper
import tin.thurein.domain.models.Progress
import tin.thurein.domain.models.Wonder
import tin.thurein.domain.usecases.GetWonderDetailUseCase
import javax.inject.Inject

class WonderDetailViewModel: HeaderViewModel {
    @Inject
    constructor()

    @JvmField
    @Inject
    var getWonderDetailUseCase: GetWonderDetailUseCase? = null

    val mutableLiveData: MutableLiveData<ModelWrapper<Wonder>> = MutableLiveData()

    private var wonder: Wonder? = null

    @Bindable
    fun getWonder() = wonder

    fun setWonder(wonder: Wonder) {
        this.wonder = wonder
        notifyPropertyChanged(BR.wonder)
        notifyPropertyChanged(BR.imageFile)
    }

    @Bindable
    fun getImageFile() = FileUtils.getGlobalFilePath() + wonder?.imageName

    fun getWonder(id: Long) {
        getWonderDetailUseCase?.let {
            it.getWonderById(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : SingleObserver<Wonder>{
                    override fun onSuccess(wonder: Wonder) {
                        setWonder(wonder)
                        mutableLiveData.postValue(ModelWrapper(wonder, Progress.SUCCESS, "Success"))
                    }

                    override fun onSubscribe(d: Disposable) {
                        mutableLiveData.postValue(ModelWrapper(Progress.LOADING, "Loading data..."))
                    }

                    override fun onError(e: Throwable) {
                        mutableLiveData.postValue(ModelWrapper(Progress.ERROR, "Error : ${e.message}"))
                    }
                })
        }
    }
}