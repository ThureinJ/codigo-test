package tin.thurein.codigo_test.viewModels

import androidx.lifecycle.MutableLiveData
import io.reactivex.FlowableSubscriber
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscription
import tin.thurein.domain.models.ModelWrapper
import tin.thurein.domain.models.Progress
import tin.thurein.domain.models.Wonder
import tin.thurein.domain.usecases.GetWondersUseCase
import javax.inject.Inject

class MainViewModel: HeaderViewModel {
    @Inject
    constructor()

    @JvmField
    @Inject
    var getWondersUseCase: GetWondersUseCase? = null

    val mutableLiveData: MutableLiveData<ModelWrapper<List<Wonder>>> = MutableLiveData()

    fun getWonders() {
        getWondersUseCase?.let {
            it.getWonders()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object: FlowableSubscriber<List<Wonder>>{
                    override fun onComplete() {
                    }

                    override fun onSubscribe(s: Subscription) {
                        s.request(Long.MAX_VALUE)
                        mutableLiveData.postValue(ModelWrapper(Progress.LOADING, "Retrieving wonders..."))
                    }

                    override fun onNext(t: List<Wonder>?) {
                        mutableLiveData.postValue(ModelWrapper(t, Progress.SUCCESS, "Success!"))
                    }

                    override fun onError(t: Throwable?) {
                        mutableLiveData.postValue(ModelWrapper(Progress.ERROR, "Cause : ${t?.message}."))
                    }
                })
        }
    }
}