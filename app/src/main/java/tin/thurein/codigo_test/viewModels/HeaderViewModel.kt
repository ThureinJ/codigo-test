package tin.thurein.codigo_test.viewModels

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData

open class HeaderViewModel: BaseViewModel() {
    private var title = "Codigo Test"

    private var backVisibility = View.VISIBLE

    val backPressedMutableLiveData: MutableLiveData<Int> = MutableLiveData()

    @Bindable
    fun getTitle(): String = title

    fun setTitle(str: String) {
        title = str
        notifyPropertyChanged(BR.title)
    }

    @Bindable
    fun getBackVisibility() = backVisibility

    fun setBackVisibility(visibility: Int) {
        backVisibility = visibility
        notifyPropertyChanged(BR.backVisibility)
    }

    fun onBackPressed() {
        backPressedMutableLiveData.postValue(1)
    }
}