package tin.thurein.codigo_test.viewModels

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import tin.thurein.data.common.FileUtils
import tin.thurein.domain.models.Wonder
import java.io.File

class WonderViewModel: BaseViewModel() {
    private var wonder: Wonder? = null

    @Bindable
    fun getWonder() = wonder

    fun setWonder(wonder: Wonder) {
        this.wonder = wonder
        notifyPropertyChanged(BR.wonder)
        notifyPropertyChanged(BR.imageFile)
    }

    @Bindable
    fun getImageFile(): String = wonder?.imageUrl!!
}