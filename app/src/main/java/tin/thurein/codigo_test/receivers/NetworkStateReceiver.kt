package tin.thurein.codigo_test.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import tin.thurein.codigo_test.common.NetworkUtils
import tin.thurein.codigo_test.delegates.NetworkStateDelegate
import tin.thurein.codigo_test.models.NetworkState

class NetworkStateReceiver : BroadcastReceiver() {

    var networkStateDelegate: NetworkStateDelegate? = null

    override fun onReceive(context: Context, intent: Intent) {
        networkStateDelegate?.let {
            it.onNetworkChanged(
                if (NetworkUtils.isNetworkAvailable(context)) NetworkState.CONNECTED
                else NetworkState.NOT_CONNECTED
            )
        }
    }
}
