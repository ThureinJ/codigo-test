package tin.thurein.codigo_test.common

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import tin.thurein.codigo_test.R

object BindingUtils {
    @JvmStatic
    @BindingAdapter(value = ["app:filePath"], requireAll = false)
    fun setImage(iv: ImageView, filePath: String?) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
        Glide.with(iv.context).load(filePath).apply(options).into(iv)
    }
}