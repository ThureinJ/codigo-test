package tin.thurein.codigo_test.delegates

import tin.thurein.codigo_test.models.NetworkState

interface NetworkStateDelegate {
    fun onNetworkChanged(networkState: NetworkState)
}