package tin.thurein.codigo_test.delegates

interface WonderAdapterDelegate {
    fun onItemClick(position: Int)
}