package tin.thurein.codigo_test.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import tin.thurein.codigo_test.R
import tin.thurein.codigo_test.databinding.WonderDetailLayoutBinding
import tin.thurein.codigo_test.delegates.WonderAdapterDelegate
import tin.thurein.codigo_test.viewModels.WonderViewModel
import tin.thurein.domain.models.Wonder

class WonderAdapter(private val delegate: WonderAdapterDelegate): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var wonders: List<Wonder> = ArrayList()

    fun setWonders(wonders: List<Wonder>) {
        this.wonders = wonders
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater: LayoutInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: WonderDetailLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.wonder_detail_layout, parent, false)
        binding.vm = WonderViewModel()
        val viewHolder = WonderAdapterViewHolder(binding)
        viewHolder.itemView.setOnClickListener { delegate.onItemClick(viewHolder.adapterPosition) }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return wonders.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is WonderAdapterViewHolder) {
            holder.bind(wonders[position])
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        if (holder is WonderAdapterViewHolder) {
            holder.unbind()
        }
    }

    inner class WonderAdapterViewHolder(private val binding: WonderDetailLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(wonder: Wonder) {
            binding.vm?.setWonder(wonder)
            binding.invalidateAll()
        }

        fun unbind() {
            binding.unbind()
        }
    }
}